package sonartest.mibanco.testfailedsonar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        codigoRepertido1(1,1);
        codigoRepertido2(1,1);
        codigoRepertido3(1,1);
        codigoRepertido4(1,1);
        codigoRepertido5(1,1);
        codigoRepertido6(1,1);
        codigoRepertido7(1,1);
        codigoRepertido8(1,1);
        codigoRepertido9(1,1);
        codigoRepertido10(1,1);
        codigoRepertido11(1,1);
        codigoRepertido12(1,1);
        codigoRepertido13(1,1);
        codigoRepertido14(1,1);
        codigoRepertido15(1,1);
        codigoRepertido16(1,1);
        codigoRepertido17(1,1);
        codigoRepertido18(1,1);
        codigoRepertido19(1,1);
        codigoRepertido20(1,1);
        codigoRepertido21(1,1);
        codigoRepertido22(1,1);
        codigoRepertido23(1,1);
        codigoRepertido24(1,1);
        codigoRepertido25(1,1);
        codigoRepertido26(1,1);

        codigo1();
        codigo2();
        codigo3();
        codigo4();

        Object a = null;
        a.equals(new Object());
    }

    private void codigo1(){
        codigoRepertido1(1,1);
        codigoRepertido2(1,1);
        codigoRepertido3(1,1);
        codigoRepertido4(1,1);
        codigoRepertido5(1,1);
        codigoRepertido6(1,1);
        codigoRepertido7(1,1);
        codigoRepertido8(1,1);
        codigoRepertido9(1,1);
        codigoRepertido10(1,1);
        codigoRepertido11(1,1);
        codigoRepertido12(1,1);
        codigoRepertido13(1,1);
        codigoRepertido14(1,1);
        codigoRepertido15(1,1);
        codigoRepertido16(1,1);
        codigoRepertido17(1,1);
        codigoRepertido18(1,1);
        codigoRepertido19(1,1);
        codigoRepertido20(1,1);
        codigoRepertido21(1,1);
        codigoRepertido22(1,1);
        codigoRepertido23(1,1);
        codigoRepertido24(1,1);
        codigoRepertido25(1,1);
        codigoRepertido26(1,1);
    }

    private void codigo2(){
        codigoRepertido1(1,1);
        codigoRepertido2(1,1);
        codigoRepertido3(1,1);
        codigoRepertido4(1,1);
        codigoRepertido5(1,1);
        codigoRepertido6(1,1);
        codigoRepertido7(1,1);
        codigoRepertido8(1,1);
        codigoRepertido9(1,1);
        codigoRepertido10(1,1);
        codigoRepertido11(1,1);
        codigoRepertido12(1,1);
        codigoRepertido13(1,1);
        codigoRepertido14(1,1);
        codigoRepertido15(1,1);
        codigoRepertido16(1,1);
        codigoRepertido17(1,1);
        codigoRepertido18(1,1);
        codigoRepertido19(1,1);
        codigoRepertido20(1,1);
        codigoRepertido21(1,1);
        codigoRepertido22(1,1);
        codigoRepertido23(1,1);
        codigoRepertido24(1,1);
        codigoRepertido25(1,1);
        codigoRepertido26(1,1);
    }

    private void codigo3(){
        codigoRepertido1(1,1);
        codigoRepertido2(1,1);
        codigoRepertido3(1,1);
        codigoRepertido4(1,1);
        codigoRepertido5(1,1);
        codigoRepertido6(1,1);
        codigoRepertido7(1,1);
        codigoRepertido8(1,1);
        codigoRepertido9(1,1);
        codigoRepertido10(1,1);
        codigoRepertido11(1,1);
        codigoRepertido12(1,1);
        codigoRepertido13(1,1);
        codigoRepertido14(1,1);
        codigoRepertido15(1,1);
        codigoRepertido16(1,1);
        codigoRepertido17(1,1);
        codigoRepertido18(1,1);
        codigoRepertido19(1,1);
        codigoRepertido20(1,1);
        codigoRepertido21(1,1);
        codigoRepertido22(1,1);
        codigoRepertido23(1,1);
        codigoRepertido24(1,1);
        codigoRepertido25(1,1);
        codigoRepertido26(1,1);
    }

    private void codigo4(){
        codigoRepertido1(1,1);
        codigoRepertido2(1,1);
        codigoRepertido3(1,1);
        codigoRepertido4(1,1);
        codigoRepertido5(1,1);
        codigoRepertido6(1,1);
        codigoRepertido7(1,1);
        codigoRepertido8(1,1);
        codigoRepertido9(1,1);
        codigoRepertido10(1,1);
        codigoRepertido11(1,1);
        codigoRepertido12(1,1);
        codigoRepertido13(1,1);
        codigoRepertido14(1,1);
        codigoRepertido15(1,1);
        codigoRepertido16(1,1);
        codigoRepertido17(1,1);
        codigoRepertido18(1,1);
        codigoRepertido19(1,1);
        codigoRepertido20(1,1);
        codigoRepertido21(1,1);
        codigoRepertido22(1,1);
        codigoRepertido23(1,1);
        codigoRepertido24(1,1);
        codigoRepertido25(1,1);
        codigoRepertido26(1,1);
    }

    private int codigoRepertido1(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;



        return c;
    }


    private int codigoRepertido2(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido3(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido4(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido5(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido6(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido7(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido8(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido9(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido10(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido11(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido12(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido13(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido14(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido15(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido16(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido17(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido18(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }


    private int codigoRepertido19(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido20(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido21(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido22(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido23(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido24(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido25(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

    private int codigoRepertido26(int a, int b){
        int c;

        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;
        c = (a+b)*2;

        return c;
    }

}
